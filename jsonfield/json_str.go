package jsonfield

import "encoding/json"

func NewStrJson[T any](val T) StrJson[T] {
	return StrJson[T]{val: val}
}

func NewJsonStrAny(val any) StrJson[any] {
	return StrJson[any]{val: val}
}

func NewStrJsonPtr[T any](val T) *StrJson[T] {
	return &StrJson[T]{val: val}
}

func NewJsonStrAnyPtr(val any) *StrJson[any] {
	return &StrJson[any]{val: val}
}

type StrJson[T any] struct {
	val T
}

func (j StrJson[T]) Unwrap() T {
	return j.val
}

func (j StrJson[T]) StringE() (string, error) {
	strbody, err := json.Marshal(j.val)
	if err != nil {
		return "", err
	}
	return string(strbody), nil
}

func (j StrJson[T]) String() string {
	strbody, _ := json.Marshal(j.val)
	return string(strbody)
}

func (j *StrJson[T]) Set(val T) {
	j.val = val
}

func (j StrJson[T]) MarshalJSON() ([]byte, error) {
	strbody, err := json.Marshal(j.val)
	if err != nil {
		return nil, err
	}

	return json.Marshal(string(strbody))
}

func (j *StrJson[T]) UnmarshalJSON(data []byte) error {
	strbody := ""
	if err := json.Unmarshal(data, &strbody); err != nil {
		return err
	}
	return json.Unmarshal([]byte(strbody), &j.val)
}
