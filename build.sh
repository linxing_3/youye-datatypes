#!/bin/bash 


cmd=$1

cmd_publish="publish" # 发布命令

function publish() {
    version=$2
    commit=$3


    if [[ $version == "" ]];then 
        read -p "请输入版本号: " version
    fi

    if [[ $commit == "" ]];then 
        read -p "请输入提交信息: " commit
    fi


    if [ "$commit" != "" ]; then
        git add .
        git commit -m "$commit"
    fi

    nowDate=$(date "+%Y%m%d%H%M")
    echo "tag: $version-$nowDate"
    git tag "$version-$nowDate"
    git push
    git push --tags 
}

if [ "$cmd" == "$cmd_publish" ]; then
    publish $@
fi

